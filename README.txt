﻿===============================================
A MATLAB class library for Tobii Pro Glasses 2
===============================================

A MATLAB controller for accessing eye-tracker data and managing recordings,
participants and calibrations using the wearable Tobii Pro Glasses 2.



Library installation
=======================

All you have to do is copy this folder or only the TobiiProGlasses2.m file 
to your project folder and do not forget to add its path to MATLAB search path.


This library is required Java Virtual Machine (JVM™), but it is included 
in MATLAB install package.
So you can check JVM™ version on MATLAB.

    >> version -java



Tobii Pro Glasses 2 Connection
==============================

Connect the glasses through Wifi or Ethernet connection. 

In case you have multiple network interfaces in your system, please specify 
the network interface name when you initiate a TobiiGlassesController object:

    % You should specify the net interface index
    tobii = TobiiProGlasses2();      % create TobiiProGlasses2 object
    tobii.init();                    % initialize and connect to device



Examples
=========

The following Python examples are available in the directory 'examples':

    - connect.m - A MATLAB script to connect with the eye-tracker.
    - calibrate_and_record.m - A MATLAB script for managing calibrations and recordings.
    - streaming.m - A MATLAB script for receiving live data from the eye-tracker.
    - streaming_fast.m - A MATLAB script for receiving live data fast from the eye-tracker.



Usage
======

Read help documents (select the class name (TobiiProGlasses2) and press F1), and see examples. 


Here is lists of public properties and methos.

properties list:
  get only
    - interface;            % wlan0 or eth0, wifi or wired, IPv4 or IPv6
    - address;              % IP address of TobiiProGlasses2
    - baseUrl;              % base URL for REST API
    - data;                 % Temporary storage for streaming data struct. For further info, see getData() description. 
    - streaming;            % is streaming or not (true/false)
    - lastSentTime;         % last time keep alive message was sent
    - recNum;               % identification number for recording
    - dataSocket;           % a data socket to communicate with device
    - javaReceiver;         % is a java instance of RecvData class 
  get and set
    - wlanSSID;             % SSID of available wlan
    - wlanKey;              % Key of available wlan
    - discoverTimeout;      % := 1 sec.
    - keepAliveTimeout;     % := 1 sec. see: /api/system/conf >> sys_livectrl_ka (msec) *3
    - packetLength;         % := 4096 bytes. for receiving streaming data
    - calibrationTimeout;   % := 15 sec.
    - recvFuncType;         % type of function to receive streaming data

methods list:
    - TobiiProGlasses2()    % Create a handle object (instance) of this class.
    - init()                % Initialize and connect to the device.
    - close()               % Disconnect to the device and close.
    - checkStatus()         % Check status of device using REST API.
    - startStreaming()      % Start data streaming.
    - stopStreaming()       % Stop data streaming.
    - isStreaming()         % Return data streaming is on or off.
    - getProjectID()        % Get a project ID from its project name.
    - getParticipantID()    % Get a participant ID from its participant name.
    - getStatus()           % Get status of Tobii Pro Glasses 2
    - getBatteryStatus()
    - getBatteryLevel()
    - getBatteryRemainingTime()
    - getBatteryInfo()
    - getStorageStatus()
    - getStorageRemainingTime()
    - getStorageInfo()
    - getRecordingStatus()
    - isRecording()         % Return whether a recording is on going or not.
    - getCurrentRecordingID()
    - createProject()       % Create a new project.
    - createParticipant()   % Create a new participant in a project.
    - createCalibration()   % Create a new calibration linked to a participant in a project.
    - startCalibration()    % Start a new calibration.
    - waitForCalibration()  % Wait until the calibration is done or timeout.
    - createRecording()     % Create a new recording in a project.
    - waitForRecordingStatus()  % Wait until the recordong status turns to a specific state or timeout.
    - startRecording()      % Start a new recording in a project.
    - stopRecording()       % Finish the ongoing recording.
    - pauseRecording()      % Pause the ongoing recording.
    - sendEvent()
    - switchWlanMode()
    - checkInternalClock()
    - setInternalClock()    % under construction
    - getData()             % Get a struct of latest streaming data.



Note
=====

This library is developed and tested in following environment. 
    PC
        OS: Microsoft Windows 10 Home 64bit
    MATLAB
        version: 9.3.0.713579 (R2017b)
        Java Version: Java 1.8.0_121-b13 with Oracle Corporation Java HotSpot(TM) 64-Bit Server VM mixed mode
    TobiiProGlasses2
        sys_version: '1.25.3-citronkola'
        sys_api_version: '1.1.0'


The code is modified from TobiiProGlasses2_PyCtrl-1.1.5, available at:

    https://github.com/ddetommaso/TobiiProGlasses2_PyCtrl
        or 
    $ pip install tobiiglassesctrl


And the code is based on the Tobii Pro Glasses 2 API_1.23.0 available at:

    https://www.tobiipro.com/product-listing/tobii-pro-glasses-2-sdk/



Contact
========

If you find bugs or errors, please tell me.

Seiji Oshiro : seios3566[at]gmail.com