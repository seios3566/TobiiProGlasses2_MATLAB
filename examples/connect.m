function connect()

    tobii = TobiiProGlasses2();      % create TobiiProGlasses2 object
    tobii.init();                    % connect to device

    fprintf('Press a key to disconnect.\n');
    pause

    tobii.stopStreaming();
    tobii.close();

end