function streaming_fast()
    global globalTobii                            % declare 'global' to use the same object in java function in startStreaming()
    globalTobii = TobiiProGlasses2();             % create TobiiProGlasses2 object
    globalTobii.init();                           % initialize and connect to the device

    fprintf('Press a key to start streaming (100 samples will be shown)\n');
    funcType = input('Select function type. (receive:1 / fetch:2) : '); % see the description below
    globalTobii.startStreaming(funcType);         % fast grabbing data option
    pause(1);

    switch funcType
        case 1  % Receive Type
            %   In case of Receive type, obj.data (streaming data) will be updated automatically and
            % rapidly (about every 7ms). So in order to get latest streaming data, you need only access
            % obj.data directly or use obj.getData() function.
            %   However, due to some technical problems, this function will receive not all the data that
            % Tobii Glasses sends but will drop about 20% of the data, though this is more efficient
            % than the normal type of obj.startStreaming() function. 
            %   Additionally, this type will be STOPPED in a while loop or a for loop because MATLAB
            % restricts the usage to multithread (parallel) computing. If you access to obj.data
            % during a loop process, the returned data is not latest. One solution is to use 'timer'
            % instead of using a loop.
            %   This is why, it is recommend to use this function only when obj.data cannot be updated
            % frequently using Fetch Type or when you want to save time to fetch data for some reason.

            t = timer;
            t.TimerFcn = @(~,~) getAndDispData();
            t.Period = 1/100;
            t.ExecutionMode = 'fixedRate';
            t.TasksToExecute = 100;
            start(t);

            pause(1);

            stop(t);
            delete(t);
            pause(1);

        case 2  % Fetch Type
            %   In case of Fetch type, obj.data (streaming data) will NOT be updated automatically. To
            % update and get latest streaming data, you have to use obj.getData(). This is the only way
            % to get latest data, so even if you access to obj.data directly, you can only get old data.
            %   In contrast to Receive type, this type can be used in loops because it receive data
            % syncronously but not parallel. However, it is assumed that this type of function is uesd
            % in a rapid loop, so this function wlii update some but not all of the types of data at one
            % time not to takes too long time to execute. It takes at longest 10ms around to execute and
            % fortunately this function will drop at most a few percents of the data that Tobii Glasses
            % sends if you use this in a rapid loop.

            for i = 1:100
                getAndDispData();
            end
    end

    globalTobii.stopStreaming();
    globalTobii.close();

    function getAndDispData()
        tobiiData = globalTobii.getData();
        fprintf('Head Unit: %s\n\n', jsonencode(tobiiData.('mems')));
        fprintf('Left Eye: %s\n\n', jsonencode(tobiiData.('left_eye')));
        fprintf('Right Eye: %s\n\n', jsonencode(tobiiData.('right_eye')));
        fprintf('Gaze Position: %s\n\n', jsonencode(tobiiData.('gp')));
        fprintf('Gaze Position 3D: %s\n\n', jsonencode(tobiiData.('gp3')));
    end
end
