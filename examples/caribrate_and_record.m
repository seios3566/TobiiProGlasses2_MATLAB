function caribrate_and_record()
    tobii = TobiiProGlasses2();             % create TobiiProGlasses2 object
    tobii.init();                           % initialize and connect to the device

    disp(tobii.getBatteryInfo());
    disp(tobii.getStorageInfo());
    
    if tobii.isRecording()
        recID = tobii.getCurrentRecordingID();
        tobii.stopRecording(recID);
    end

    projectName = input('\nPlease insert the project''s name: ','s');
    projectID = tobii.createProject(projectName);

    participantName = input('Please insert the participant''s name: ','s');
    participantID = tobii.createParticipant(projectID, participantName);

    calibrationID = tobii.createCalibration(projectID, participantID);
    input('Put the calibration marker in front of the user, then press enter to calibrate.','s');
    tobii.startCalibration(calibrationID);
    calibrationTimeout = 15;        %sec.
    responce = tobii.waitForCalibration(calibrationID, calibrationTimeout);

    if ~responce
        fprintf('Calibration Failed!, using default calibration instead.\n\n');
        % tobii.close();
    end

    recordingID = tobii.createRecording(projectID, participantID, calibrationID);
    fprintf('The recording will be stored in the SD folder projects/%s/recordings/%s\n', projectID, recordingID);
    input('Press enter to start recording for 10 seconds.\n');
    responce = tobii.startRecording(recordingID);

    if ~responce
        tobii.close();
        error('Failed to start recording! \n');
    else
        fprintf('Recording started...\n\n');
        for i = flip(1:10)
            disp(i);
            pause(1);
        end
        responce = tobii.stopRecording(recordingID);
    end
    
    if ~responce
        fprintf('Recording failed. \n\n');
    else
        fprintf('Recording finished.\n\n');
    end

    tobii.close();
    
end