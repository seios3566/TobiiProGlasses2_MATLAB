function streaming()
    % Please see comments in streaming_fast.m !!

    tobii = TobiiProGlasses2();             % create TobiiProGlasses2 object
    tobii.init();                           % initialize and connect to the device

    input('Press a key to start streaming (100 samples will be shown)\n');
    tobii.startStreaming(0);
    pause(1);
       
    for i = 1:100
        tobiiData = tobii.getData();
        fprintf('\n\n i = %d\n', i);
        fprintf('Head Unit: %s\n\n', jsonencode(tobiiData.('mems')));
        fprintf('Left Eye: %s\n\n', jsonencode(tobiiData.('left_eye')));
        fprintf('Right Eye: %s\n\n', jsonencode(tobiiData.('right_eye')));
        fprintf('Gaze Position: %s\n\n', jsonencode(tobiiData.('gp')));
        fprintf('Gaze Position 3D: %s\n\n', jsonencode(tobiiData.('gp3')));
pause(0.1);
    end
    
    tobii.stopStreaming();
    tobii.close();
    
end