classdef TobiiProGlasses2_old < handle
    % TobiiProGlasses2.m : A MATLAB class library for Tobii Pro Glasses 2
    % Usege : see README.txt or examples
    %
    %  Copyright (C) 2018  Seiji Oshiro
    %
    % This library is modified from a python package, tobiiglassesctrl
    % The project is available on Github at:
    %       https://github.com/ddetommaso/TobiiProGlasses2_PyCtrl
    %
    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program. If not, see <http://www.gnu.org/licenses/>
    
    
    properties (SetAccess = private)
        ipType;             % IPv4 or IPv6
        baseUrl;            % base URL for REST API
        data;               % Temporary storage for streaming data struct. For further info, see getData() description. 
        streaming;          % is streaming or not (true/false)
        lastSentTime;       % last time keep alive message was sent
        recNum;             % identification number for recording
        dataSocket;         % a data socket to communicate with device
        javaReceiver;       % is a java instance of RecvData class 

        % videoSocket;
        % eyesSocket;
    end

    properties
        host;               % host(TobiiGlasses) IP address
        wlanSSID;           % SSID of available wlan
        wlanKey;            % Key of available wlan
        discoverTimeout;    % := 1 sec.
        keepAliveTimeout;   % := 1 sec. see: /api/system/conf >> sys_livectrl_ka (msec) *3
        packetLength;       % := 4096 bytes. for receiving streaming data
        calibrationTimeout; % := 15 sec.
        recvFuncType;       % type of function to receive streaming data
    end

    properties (Constant)
        v4ADDR = '192.168.71.50';       % on wi-fi network
        PORT = 49152;                   % for both IPv4 and IPv6
        
        v6MULTICAST_ADDR = 'ff02::1';   % ipv6: all nodes on the local network segment
        v6MULTICAST_PORT = 13006;       % 
        
        KA_DATA_MSG = '{"type": "live.data.unicast", "key": "dataGUID", "op": "start"}';

        % KA_VIDEO_MSG = '{"type": "live.video.unicast", "key": "videoGUID", "op": "start"}';
        % KA_EYES_MSG = '{"type": "live.eyes.unicast", "key": "eyeGUID", "op": "start"}';
    end
    
    
    
    methods
        function obj = TobiiProGlasses2_old()
            % Create a handle object (instance) of this class.
            % 
            % Syntax:
            %       obj = TobiiProGlasses2()
            % 

            obj.data = {};
            noData = '{"ts": -1}';
            obj.data.('mems') = jsondecode(sprintf('{"ac" : %s, "gy" : %s}', noData, noData));
            obj.data.('right_eye') = jsondecode(sprintf('{"pc" : %s, "pd" : %s, "gd" : %s}', noData, noData, noData));
            obj.data.('left_eye') = jsondecode(sprintf('{"pc" : %s, "pd" : %s, "gd" : %s}', noData, noData, noData));
            obj.data.('gp') = jsondecode(noData);
            obj.data.('gp3') = jsondecode(noData);

            obj.streaming = false;
            obj.recNum = 0;
            obj.discoverTimeout = 1;        % 0 <= int <= 255
            obj.keepAliveTimeout = 1;       % sec.
            obj.packetLength = 1024;        % bytes
            obj.calibrationTimeout = 15;    % sec.
        end
        
        function init(obj, access)
            % Initialize and connect to the device.
            % 
            % Syntax:
            %       obj.init([access])
            %   or
            %       init(obj[, access])
            %   'access' - a string to specify its interface connected to the Tobii Pro Glasses 2,
            % please input 'wifi'/'wireless', or 'wired'.
            % 
            % Usage:
            %   After created a new instance of TobiiProGlasses2 class,
            %       tobii.init('wifi');            % wireless network
            %       tobii.init('wireless');        % this works in the same way as 'wifi'
            %       tobii.init('wired');           % in case the device is connected with a LAN cable
            %       tobii.init();                  % this usage is ok, but not recommended
            % 
            
            fprintf('\nStart initializing and connecting to the device...\n');
            switch nargin
                case 1
                    obj.ipType = '';
                case 2
                    switch access
                        case {'wifi', 'wireless'}
                            obj.ipType = 'v4';
                        case 'wired'
                            obj.ipType = 'v6';
                        otherwise
                            obj.ipType = '';
                    end
                otherwise
                    error('too many input arguments!');
            end
            
            obj.connect();
            wlanStatus = obj.checkStatus('/api/system/wlan');
            obj.wlanSSID = wlanStatus.('sys_wlan_client_ssid');
            obj.wlanKey = wlanStatus.('sys_wlan_client_key');
            obj.checkInternalClock();
        end
        
        function close(obj)
            % Disconnect to the device and close.
            % like __del__() and __disconnect__() in tobiiglassesctrl lib.
            % 
            % Syntax:
            %       obj.close()
            % 
            
            if ~isempty(obj.ipType)
                fprintf('Disconnecting to the Tobii Pro Glasses 2\n');
                if obj.streaming
                    obj.streaming = false;
                    obj.stopStreaming();
                end
                if obj.isRecording()
                    recID = obj.getCurrentRecordingID();
                    responce = obj.stopRecording(recID);
                    if ~responce
                        fprintf('Recording failed. \n\n');
                    else
                        fprintf('Recording succeed.\n\n');
                    end
                end
                fprintf('Tobii Pro Glasses 2 successful disconnected!\n\n');
            end
            fprintf('Tobii Pro Glasses 2 successful closed!\n\n');
        end
        
        function result = checkStatus(obj, apiAction, key, valueCellArray)
            % Check status of device using REST API.
            % 
            % Syntax:
            %       result = obj.checkStatus(apiAction[, key[, valueCellArray]])
            % 
            %   'apiAction' - a string of url
            %   'key' - a string of the key (field) should be checked.
            %   'valueCellArray' - a cell array of the values.
            %                      Make sure to input value strings as a cell array (with ',' delimiter and enclosed with {})
            % 
            %   'result' - When input the key argument, result is value, otherwise false (0). 
            %              When the key argument is omitted, this method returns the raw responce from REST API.
            % 
            % Usage:
            % 
            
            res = obj.urlRequest(apiAction);
            switch nargin
                case 2
                    result = res;
                case 3
                    try
                        keys = split(key, '.');
                        if length(keys) == 1
                            result = res.(keys{1});
                        elseif length(keys) == 2
                            result = res.(keys{1}).(keys{2});
                        elseif length(keys) == 3
                            result = res.(keys{1}).(keys{2}).(keys{3});
                        end
                    catch
                        result = false;
                        fprintf('Not found such a key!\n');
                    end
                case 4
                    try
                        keys = split(key, '.');
                        if length(keys) == 1
                            value = res.(keys{1});
                        elseif length(keys) == 2
                            value = res.(keys{1}).(keys{2});
                        elseif length(keys) == 3
                            value = res.(keys{1}).(keys{2}).(keys{3});
                        end
                        for i = 1:length(valueCellArray)
                            if strcmp(value, valueCellArray(i))
                                result = value;
                                break;
                            else
                                result = false;
                            end
                        end
                    catch
                        result = false;
                    end
                otherwise
                    result = false;
                    error('Invalid number of input arguments!\n');
            end     % switch
        end
        
        function startStreaming(obj, funcType)
            % Start data streaming.
            % 
            % Syntax:
            %       obj.startStreaming([funcType])
            %   
            %   'funcType' - 0 : normal streaming (using MATLAB timer)
            %                1 : enable fast receiving data (Receive Type) using java class method
            %                2 : enable fast receiving data (Fetch Type) using java class method
            %
            %           Receive Type :
            %               In case of Receive type, obj.data (streaming data) will be updated automatically and
            %             rapidly (about every 7ms). So in order to get latest streaming data, you need only access
            %             obj.data directly or use obj.getData() function.
            %               However, due to some technical problems, this function will receive not all the data that
            %             Tobii Glasses sends but will drop about 20% of the data, though this is more efficient
            %             than the normal type of obj.startStreaming() function. 
            %               Additionally, this type will be STOPPED in a while loop or a for loop because MATLAB
            %             restricts the usage to multithread (parallel) computing. If you access to obj.data
            %             during a loop process, the returned data is not latest. One solution is to use 'timer'
            %             instead of using a loop.
            %               This is why, it is recommend to use this function only when obj.data cannot be updated
            %             frequently using Fetch Type or when you want to save time to fetch data for some reason.
            % 
            %            Fetch Type :
            %               In case of Fetch type, obj.data (streaming data) will NOT be updated automatically. To
            %             update and get latest streaming data, you have to use obj.getData(). This is the only way
            %             to get latest data, so even if you access to obj.data directly, you can only get old data.
            %               In contrast to Receive type, this type can be used in loops because it receive data
            %             syncronously but not parallel. However, it is assumed that this type of function is uesd
            %             in a rapid loop, so this function wlii update some but not all of the types of data at one
            %             time not to takes too long time to execute. It takes at longest 10ms around to execute and
            %             fortunately this function will drop at most a few percents of the data that Tobii Glasses
            %             sends if you use this in a rapid loop.
            % 
            % Usage:
            %   When the device connected, streaming is ready.
            %   
            %   After started streaming, obj.getData() returns received data, or you can access obj.data directory to get.
            %   To finish streaming, use obj.stopStreaming().
            % 
            
            fprintf('Start data streaming ...\n');
            if nargin == 1 || funcType < 0 || funcType > 2
                funcType = 0;
            end
            try
                obj.streaming = true;
                obj.recvFuncType = funcType;
                obj.dataSocket = obj.mkSock();
                obj.sendKeepAliveMsg();
                pause(0.5);
                if obj.recvFuncType
                    if isempty(which('RecvData'))         
                        javaaddpath([fileparts(mfilename('fullpath')) '\java\recvDataFast.jar']);
                        if isempty(which('RecvData')) 
                            error('Please javaaddpath() recvDataFast.jar file!');
                        end
                    end
                    strObj = inputname(1);
                    gVars = whos('global');
                    for i = 1:length(gVars)
                        if strcmp(strObj, gVars(i).name) && strcmp('TobiiProGlasses2', gVars(i).class)
                            obj.keepRecvDataFast(strObj, obj.recvFuncType);
                        elseif strcmp(strObj, gVars(i).name)
                            warning('Declare global in your script for fast mode! Start streaming as normal mode.');
                            obj.keepRecvData();
                        end
                    end                    
                else
                    obj.keepRecvData();                    
                end
                fprintf('Data streaming successful started!\n');
            catch ME
                obj.streaming = false;
                obj.close();
                fprintf('An error occurs trying to start data streaming.');
                rethrow(ME);
            end % try...catch
        end
        
        function stopStreaming(obj)
            % Finish data streaming.
            % 
            % Syntax:
            %       obj.stopStreaming()
            % 
            
            fprintf('Stop data streaming ...\n');
            try
                if obj.streaming
                    obj.streaming =false;
                    obj.dataSocket.close;
                    if ~isempty(obj.javaReceiver)
                        obj.javaReceiver.stop();
                    end
                    stop(timerfind());      % to stop sending keep alive msg
                    delete(timerfind());
                end
                fprintf('Data streaming successful stopped!\n');
            catch
                obj.close();
                error('An error occurs trying to stop data streaming.\n');
            end % try...catch
        end

        function result = isStreaming(obj)
            % Return data streaming is on or off.
            % 
            % Syntax:
            %       result = obj.isStreaming()
            % 
            %   'result' is true (1) or false (0)
            % 
            % Usage:
            % 

            result = obj.streaming;
        end
        
        function projectID = getProjectID(obj, projectName)
            % Get a project ID from its project name.
            % 
            % Syntax:
            %       projectID = obj.getProjectID(obj, projectName)
            % 
            %   'projectName' - a string to specify a project to look for
            % 
            %   'projectID' - an unique string to identify a project
            % 

            projectID = '';
            projects = obj.urlRequest('/api/projects');
            for i = 1:length(projects)
                if iscell(projects) && isfield(projects{i}, 'pr_info')
                    if strcmp(projects{i}.('pr_info').('Name'), projectName)
                        projectID = projects{i}.('pr_id');
                    end
                elseif isstruct(projects) && isfield(projects, 'pr_info')
                    if strcmp(projects(i).('pr_info').('Name'), projectName)
                        projectID = projects(i).('pr_id');
                    end
                end
            end
        end
        
        function participantID = getParticipantID(obj, participantName, projectID)
            % Get a participant ID from its participant name.
            % 
            % Syntax:
            %       participantID = obj.getparticipantID(participantName)
            % 
            %   'participantName' - a string to specify a participant to look for
            %   'projectID' - a string to specify a project which has a participant named participantName.
            % 
            %   'participantID' - an unique string to identify a participant data
            % 

            participantID = '';
            if nargin == 2
                participants = obj.urlRequest('/api/participants');
            elseif nargin == 3
                participants = obj.urlRequest(['/api/projects/' projectID '/participants']);
            end
            for i = 1:length(participants)
                if iscell(participants) && isfield(participants{i}, 'pa_info')
                    if strcmp(participants{i}.('pa_info').('Name'), participantName)
                        participantID = participants{i}.('pa_id');
                    end
                elseif isstruct(participants) && isfield(participants, 'pa_info')
                    if strcmp(participants(i).('pa_info').('Name'), participantName)
                        participantID = participants(i).('pa_id');
                    end
                end
            end
        end
        
        function status = getStatus(obj)
            % Get status of Tobii Pro Glasses 2
            % 
            % Syntax:
            %       status = obj.getStatus()
            % 
            %   'status' - a return variable in json format
            % 
            % Usage:
            %   This method will access to /api/system/status
            %   and return json responces from REST API
            % 

            status = obj.urlRequest('/api/system/status');
        end
        
        function battery = getBatteryStatus(obj)
            battery = obj.getStatus().('sys_battery');
        end
        
        function batteryLevel = getBatteryLevel(obj)
            batteryLevel = obj.getBatteryStatus().('level');
        end
        
        function batteryTime = getBatteryRemainingTime(obj)
            batteryTime = obj.getBatteryStatus().('remaining_time');
        end
        
        function batteryInfo = getBatteryInfo(obj)
            batteryInfo = sprintf('Battery Info = [Level: %d%%, RemainingTime: %dh%dm]' , obj.getBatteryLevel(), fix(obj.getBatteryRemainingTime()/3600), fix(mod(obj.getBatteryRemainingTime(), 3600)/60));
        end
        
        function storage = getStorageStatus(obj)
            storage = obj.getStatus().('sys_storage');
        end
        
        function storageTime = getStorageRemainingTime(obj)
            storageTime = obj.getStorageStatus().('remaining_time');
        end
        
        function storageInfo = getStorageInfo(obj)
            storageInfo = sprintf('Storage Info = [RemainingTime : %dh%dm]', fix(obj.getStorageRemainingTime()/3600), fix(mod(obj.getStorageRemainingTime(), 3600)/60));
        end
        
        function recStatus = getRecordingStatus(obj)
            recStatus = obj.getStatus().('sys_recording');
        end
        
        function result = isRecording(obj)
            % Return whether a recording is on going or not.
            % 
            % Syntax:
            %       result = obj.isRecording()
            % 
            %   'result' is true (1) or false (0)
            % 

            recStatus = obj.getRecordingStatus();
            if isfield(recStatus, 'rec_state')
                if strcmp(recStatus.('rec_state'), 'recording')
                    result = true;
                else
                    result = false;
                end
            else
                result = false;
            end
        end
        
        function recID = getCurrentRecordingID(obj)
            recID = obj.getRecordingStatus().('rec_id');
        end
        
        function projectID = createProject(obj, projectName)
            % Create a new project.
            % 
            % Syntax:
            %       projectID = obj.createProject(projectName)
            % 
            %   'projectName' - a string to represent a project.
            % 
            %   'projectID' - an unique string to identify a project data
            % 
            % Usage:
            % 

            if nargin == 1
                projectName = 'defaultProjectName';
            end
            projectID = obj.getProjectID(projectName);
            
            if isempty(projectID)
                msg = sprintf('{"pr_info" : {"CreationDate" : "%s", "EagleId" : "%s", "Name" : "%s"}}', char(datetime), char(java.util.UUID.randomUUID), projectName);
                jsonData = obj.urlRequest('/api/projects', msg);
                projectID = jsonData.('pr_id');
                fprintf('Project [%s] created!\n\n', projectID);
            else
                fprintf('Project [%s] already exists!\n\n', projectID);
                newProject = input('Start the existing project ? (y/n) : ', 's');
                if strcmp(newProject, 'y')
                    fprintf('Project [%s] is selected.\n\n', projectID);
                else
                    projectName = input('Please input the other project name. : ', 's');
                    projectID = obj.createProject(projectName);
                end
            end
        end
        
        function participantID = createParticipant(obj, projectID, participantName, participantNote)
            % Create a new participant in a project.
            % 
            % Syntax:
            %       participantID = obj.createparticipant(projectID[, participantName[, participantNote]])
            % 
            %   'projectID' - a string to specify a project
            %   'participantName' - a string to represent a participant to measure.
            %   'participantNote' - a string to descript a participant
            % 
            %   'participantID' - an unique string to identify a participant data
            % 
            % Usage:
            % 

            if nargin == 2
                participantName = 'defalutUser';
                participantNote = '';
            elseif nargin == 3
                participantNote = '';
            end
            participantID = obj.getParticipantID(participantName, projectID);
            
            if isempty(participantID)
                msg = sprintf('{"pa_project": "%s", "pa_info": {"EagleId": "%s", "Name": "%s", "Notes": "%s"}}', projectID, char(java.util.UUID.randomUUID), participantName, participantNote);
                jsonData = obj.urlRequest('/api/participants', msg);
                participantID = jsonData.('pa_id');
                fprintf('Participant [%s] created in Project [%s].\n\n', participantID, projectID);
            else
                fprintf('Participant [%s] already exists in Project [%s]!\n\n', participantID, projectID);
                newParticipant = input('Use the existing participant? (y/n) : ', 's');
                if strcmp(newParticipant, 'y')
                    fprintf('Participant [%s] in Project [%s] is selected.\n\n', participantID, projectID);
                else
                    participantName = input('Please input the other participant name : ', 's');
                    participantID = obj.createParticipant(projectID, participantName);
                end
            end
        end
        
        function calibrationID = createCalibration(obj, projectID, participantID)
            % Create a new calibration linked to a participant in a project.
            % 
            % Syntax:
            %       calibrationID = obj.createCalibration(projectID, participantID)
            % 
            %   'projectID' - a string to specify a project
            %   'participantID' - a string to specify a participant who wears the device
            % 
            %   'calibrationID' - an unique string to identify a calibration data
            % 
            % Usage:
            %   To record eye tracking data accurately, a calibration should be done before recording.
            %   After created a new calibration, you can use startCalibration() method, and then waitForCalibration() method.
            % 

            msg = sprintf('{"ca_project" : "%s", "ca_type": "defalut", "ca_participant": "%s"}', projectID, participantID);
            jsonData = obj.urlRequest('/api/calibrations', msg);
            calibrationID = jsonData.('ca_id');
            fprintf('Calibration [%s] created in Project [%s], Participant [%s]\n', calibrationID, projectID, participantID);
        end
        
        function startCalibration(obj, calibrationID)
            % Start a new calibration.
            % 
            % Syntax:
            %       obj.startCalibration(calibrationID)
            % 
            %   'calibrationID' - a string to identify a calibration
            % 
            % Usage:
            %   Before start calibration, a new calibration must be created using createCalibration().
            % 

            obj.urlRequest(['/api/calibrations/' calibrationID '/start'], '');
        end
        
        function result = waitForCalibration(obj, calibrationID, timeout)
            % Wait until the calibration is done or timeout.
            % 
            % Syntax:
            %       result = obj.waitForCalibration(calibrationID[, timeout]])
            % 
            %   'calibrationID' - a string to identify a calibration
            %   'timeout' - an integer to specify waiting time for responce in sec.
            % 
            %   'result' - true (1) indicates successfully started calibrating.
            %              false(0) indicates failure.
            % 

            statusCellArray =  {'calibrating', 'calibrated', 'stale', 'uncalibrated', 'failed'};
            if nargin == 2
                timeout = obj.calibrationTimeout;
            end
            t = tic;
            while true
                status = obj.checkStatus(['/api/calibrations/' calibrationID '/status'], 'ca_state', statusCellArray);
                if toc(t) > timeout
                    status = 'timeout';
                    break;
                elseif strcmp(status, 'calibrating')
                    continue;
                elseif status == false
                    continue;
                else
                    break;
                end
            end
            if strcmp(status, 'uncalibrated')||strcmp(status, 'stale')||strcmp(status, 'failed')
                warning('Calibration [%s] failed!\n', calibrationID);
                result = false;
            elseif strcmp(status, 'calibrated')
                fprintf('Calibration [%s] successful finished.\n', calibrationID);
                result = true;
            elseif strcmp(status, 'timeout')
                warning('Calibration [%s] timeout!\n', calibrationID);
                result = false;
            end
        end
        
        function recordingID = createRecording(obj, projectID, participantID, calibrationID, recordingName, recordingNote)
            % Create a new recording in a project.
            % 
            % Syntax:
            %       [recordingID, projectID] = obj.createRecording(projectID, participantName[, recordingName[, recordingNote]])
            % 
            %   'projectID' - a string to specify a project
            %   'participantID' - a string to specify a participant who wears the device
            %   'calibrationID' - a string to identify a calibration
            %   'recordingName' - a string to represent a recording.
            %   'recordingNote' - a string. it can be omitted.
            % 
            %   'recordingID' - an unique string to identify a recording
            %   'projectID' - a string of the projectID that has the recording
            % 
            % Usage:
            %   To record live videos, eye tracking data and other data, new recording must be created firstly.
            %   Then, you can use startRecording() method, and then pauseRecording() or stopRecording() method.
            % 
            % Note:
            %   In case the participant is not belong to the project, new recording will be created in another project that has the participant.
            % 

            if nargin == 4
                obj.recNum = obj.recNum + 1;
                recs = obj.urlRequest(['/api/participants/' participantID '/recordings']);
                for i = 1:length(recs)
                    if iscell(recs) && isfield(recs{i}, 'rec_info')
                        num = str2double(extractAfter(recs{i}.('rec_info').('Name'), 'Recording'));
                    elseif isstruct(recs) && isfield(recs, 'rec_info')
                        num = str2double(extractAfter(recs(i).('rec_info').('Name'), 'Recording'));
                    end
                    if ~isempty(num) && obj.recNum <= num
                        obj.recNum = num + 1;
                    end
                end
                recordingName = ['Recording' sprintf('%03d', obj.recNum)];
                recordingNote = '';
            elseif nargin == 5
                recordingNote = '';
            end
            msg = sprintf('{"rec_project":"%s", "rec_participant":"%s", "rec_calibration":"%s", "rec_info":{"EagleId":"%s", "Name":"%s", "Notes":"%s"}}', projectID, participantID, calibrationID, char(java.util.UUID.randomUUID), recordingName, recordingNote);
            jsonData = obj.urlRequest('/api/recordings', msg);
            recordingID = jsonData.('rec_id');
            fprintf('New Recording [%s] is created in Project [%s].\n', recordingID, projectID);
        end
        
        function status = waitForRecordingStatus(obj, recordingID, statusCellArray, timeout)
            % Wait until the recordong status turns to a specific state or timeout.
            % 
            % Syntax:
            %       status = obj.waitForRecordingStatus(recordingID[, statusCellArray[, timeout]])
            % 
            %   'recordingID' - a string to identify a recording
            %   'statusCellArray' - a cell array of strings represents status of the device.
            %                       Make sure to input value strings as a cell array (with ',' delimiter and enclosed with {})
            %   'timeout' - an integer to specify waiting time for responce in sec.
            % 
            %   'status' - one string in statusCellArray, otherwise 'timeout'. 
            % 

            if nargin == 2
                statusCellArray = {'init', 'starting',	'recording', 'pausing', 'paused', 'stopping', 'stopped', 'done', 'stale', 'failed'};
                timeout = 5;
            elseif nargin == 3
                timeout = 5;
            end
            t = tic;
            while true
                status = obj.checkStatus(['/api/recordings/' recordingID '/status'], 'rec_state', statusCellArray);
                if any(strcmp(status, statusCellArray))
                    break;
                elseif toc(t) > timeout
                    status = 'timeout';
                    break;
                end
            end
        end
        
        function result = startRecording(obj, recordingID)
            % Start a new recording in a project.
            % 
            % Syntax:
            %       result = obj.startRecording(recordingID)
            % 
            %   'recordingID' - a string to identify a recording
            % 
            %   'result' - true (1) indicates successfully started recording.
            %              false(0) indicates failure.
            % 
            % Usage:
            %   Before start recording, a new recording must be created using createRecording().
            % 

            obj.urlRequest(['/api/recordings/' recordingID '/start'], '');
            status = obj.waitForRecordingStatus(recordingID, {'recording'});
            fprintf('Recording [%s] status : %s \n', recordingID, status);
            if strcmp(status, 'recording')
                result = true;
            else
                result = false;
            end
        end
        
        function result = stopRecording(obj, recordingID)
            % Finish the ongoing recording.
            % 
            % Syntax:
            %       result = obj.stopRecording(recordingID)
            % 
            %   'recordingID' - a string to identify a recording
            % 
            %   'result' - true (1) indicates successfully stopped recording.
            %              false(0) indicates failure.
            % 

            res = obj.urlRequest(['/api/recordings/' recordingID '/stop'], '');
            status = obj.waitForRecordingStatus(recordingID, {'done'});
            if strcmp(status, 'done')
                result = true;
                projectID = res.rec_project;
                fprintf('The recording is saved in <the SD folder>/projects/%s/recordings/%s\n', projectID, recordingID);
            else
                result = false;
            end
        end
        
        function result = pauseRecording(obj, recordingID)
            % Pause the ongoing recording.
            % 
            % Syntax:
            %       result = obj.pauseRecording(recordingID)
            % 
            %   'recordingID' - a string to identify a recording
            % 
            %   'result' - true (1) indicates successfully paused recording.
            %              false(0) indicates failure.
            % 

            obj.urlRequest(['/api/recordings/' recordingID '/pause'], '');
            status = obj.waitForRecordingStatus(recordingID, {'paused'});
            if strcmp(status, 'paused')
                result = true;
            else
                result = false;
            end
        end

        function res = sendEvent(obj, eventType, eventTag)
            if nargin < 2
                error('Input the "eventType" argument!');
            end
            msg = sprintf('{"type": "%s", "tag": "%s"}', eventType, eventTag);
            res = obj.urlRequest('/api/events', msg);
        end

        function status = switchWlanMode(obj, mode, ssid, key)
            if ~any(strcmp({'ap', 'client'}, mode))
                error('Input "ap" or "client" in the mode parameter!');
            end
            if nargin == 3
                obj.wlanSSID = ssid;
            elseif nargin == 4
                obj.wlanSSID = ssid;
                obj.wlanKey = key;
            end
            if strcmp('ap', mode)
                msg = '{"sys_wlan_mode":"ap"}';
            elseif strcmp('client', mode)
                warning('NOTE : To restore to "ap" mode, the device must be connected with LAN cable!')
                msg = sprintf('{"sys_wlan_mode":"client", "sys_wlan_client_ssid":"%s", "sys_wlan_client_key":"%s"}', obj.wlanSSID, obj.wlanKey);
            end
            obj.urlRequest('/api/system/wlan', msg);

            status = obj.waitForWlanStatus({'ap', 'connected', 'not connected'});
            if any(strcmp({'not connected', 'timeout'}, status))
                error('Failed to switch to %s mode!', mode);
            else
                fprintf('Wlan mode status : %s \n', status);
            end
        end
        
        function checkInternalClock(obj)
            now = datetime('now','TimeZone','UTC');
            sysTime = datetime(obj.getStatus().sys_time, 'InputFormat', 'yyyy-MM-dd''T''HH:mm:ssXXX', 'TimeZone', 'UTC');
            if hours(now - sysTime) > 24
                warning('The Internal Clock is Wrong Time! \nNow (UTC): %s , but\nInternal time : %s', now, sysTime);
                obj.setInternalClock();
            end
        end

        function setInternalClock(obj)
            if strcmp(obj.ipType, 'v6')
                res1 = input('\nAdjust the internal time? (y/n) : ', 's');
                if strcmp(res1, 'y')
                    if ~isempty(obj.wlanSSID)
                        fprintf('\nUse [%s] wlan network?', obj.wlanSSID);
                        res2 = input('(y/n) : ', 's');
                    end
                    if isempty(obj.wlanSSID) || ~strcmp('y', res2)
                        obj.wlanSSID = input('Please input the SSID and its password of wi-fi network available now. \nSSID : ', 's');
                    end
                    if isempty(obj.wlanKey) || ~strcmp('y', res2)
                        obj.wlanKey = input('Password : ', 's');
                    end
                    status = obj.switchWlanMode('client');
                    if strcmp('connected', status)
                        fprintf('Adjusting now ...\n');
                        now = datetime('now','TimeZone','UTC');
                        while 1
                            sysTime = datetime(obj.getStatus().sys_time, 'InputFormat', 'yyyy-MM-dd''T''HH:mm:ssXXX', 'TimeZone', 'UTC');
                            if hours(now - sysTime) < 24
                                fprintf('Time successful adjusted.\n');
                                break;
                            elseif seconds(datetime('now','TimeZone','UTC') - now) > 30
                                fprintf('Time NOT adjusted.\n');
                                break;
                            end
                        end
                        obj.switchWlanMode('ap');                        
                    end
                end
            else
                warning('To adjust the time, you have to connect the divice to this computer with a LAN cable.');
            end
            fprintf('Internal time : %s\n', obj.getStatus().('sys_time'));
        end
        
        function refreshData(obj, strdata)
            % like _refresh_data_() in tobiiglassesctrl lib.
            
            jdata = jsondecode(strdata);
            
            for i = 1:length(jdata)
                if iscell(jdata)
                    jsondata = jdata{i};
                elseif isstruct(jdata)
                    jsondata = jdata(i);
                end

                % MEMS gyroscope info
                try
                    gy = jsondata.('gy');
                    ts = jsondata.('ts');
                    if obj.data.('mems').('gy').('ts') < ts
                        obj.data.('mems').('gy') = jsondata;
                    end
                catch
                end
                
                % MEMS accelerometer info
                try
                    ac = jsondata.('ac');
                    ts = jsondata.('ts');
                    if obj.data.('mems').('ac').('ts') < ts
                        obj.data.('mems').('ac') = jsondata;
                    end
                catch
                end
                
                % Pupil Center
                try
                    pc = jsondata.('pc');
                    ts = jsondata.('ts');
                    eye = jsondata.('eye');
                    if obj.data.([eye '_eye']).('pc').('ts') < ts
                        obj.data.([eye '_eye']).('pc') = jsondata;
                    end
                catch
                end
                
                % Pupil Diameter
                try
                    pd = jsondata.('pd');
                    ts = jsondata.('ts');
                    eye = jsondata.('eye');
                    if obj.data.([eye '_eye']).('pd').('ts') < ts
                        obj.data.([eye '_eye']).('pd') = jsondata;
                    end
                catch
                end
                
                % Gaze Direction
                try
                    gd = jsondata.('gd');
                    ts = jsondata.('ts');
                    eye = jsondata.('eye');
                    if obj.data.([eye '_eye']).('gd').('ts') < ts
                        obj.data.([eye '_eye']).('gd') = jsondata;
                    end
                catch
                end
                
                % Gaze Position
                try
                    gp = jsondata.('gp');
                    ts = jsondata.('ts');
                    if obj.data.('gp').('ts') < ts
                        obj.data.('gp') = jsondata;
                    end
                catch
                end
                
                % Gaze Position 3D
                try
                    gp3 = jsondata.('gp3');
                    ts = jsondata.('ts');
                    if obj.data.('gp3').('ts') < ts
                        obj.data.('gp3') = jsondata;
                    end
                catch
                end
            end
            
        end
        
        function data = getData(obj)
            % Get a struct of latest streaming data.
            % 
            % Syntax:
            %       data = obj.getData()
            % 
            %   'data' - temporary storage for streaming data struct
            % 
            % Struct:
            %   data
            %       .mems
            %           .gy                                     % Gyroscope info indicates the rotation of the glasses
            %               .gy     = [0.454,-1.149,4.125]      % the unit degrees per second[°/s].
            %               .ts     = 1114210113                % The datastream timestamp value in microseconds
            %               .s      = 0                         % A status indicator. Zero means everything is OK, any non-zero value indicate ssome kind of problem with the data. 
            %           .ac                                     % Accelerometer info indicates the rotation of the glasses
            %               .ac     = [0.454,-1.149,4.125]      % the unit meter per second squared[m/s²]. When the glasses are stationary,the value of the ac property will be approximately [0, -9,82, 0].
            %               .ts
            %               .s
            %       .right_eye                                  % see .left_eye
            %       .left_eye
            %           .pc                                     % Pupil Center
            %               .pc     = [-30,46,-21.70,-30.11]    % The property pc is specified in 3D coordinates with origo in the scenecam in mm.
            %               .eye    = "left"
            %               .gidx   = 10043                     % All messages that belong to a single eye-tracking event or gaze-event are accompanied by a gaze counter.This counter is represented by the gidx (gaze-index) property.
            %               .ts
            %               .s
            %           .pd                                     % Pupil Diameter
            %               .pd     = 3.13                      % measured in mm
            %               .eye
            %               .gidx
            %               .ts
            %               .s
            %           .gd                                     % Gaze Direction
            %               .gd     = [0.0475,0.1892,0.9808]    % a unit vector with origo in the pupil center
            %               .eye
            %               .gidx
            %               .ts
            %               .s
            %       .gp                                         % Gaze Position
            %           .gp         = [0.5004,0.3755]           % the position on the scene camera image where the gaze will be projected. Top left corner is (0,0), bottom right corner is (1,1).
            %           .l          = 281518                    % This property is the latency, expressed in a number of microseconds from the time the image is received from the camera to the time it is queued for transmitting the live data stream.
            %           .gidx
            %           .ts
            %           .s
            %       .gp3                                        % Gaze Position 3D
            %           .gp3        = [24.19,176.19,1044.84]    % the 3D position, in mm, relative to the scene camera where the gaze is focused
            %           .gidx
            %           .ts
            %           .s
            %           

            if ~isempty(obj.javaReceiver)  
                if obj.recvFuncType == 2
                    obj.javaReceiver.fetcher();
                end
            end
            data = obj.data;
        end
    end         % methods

    methods (Access = private)      % private methods

        function data = discover(obj)
            % like _discover_device_() in tobiiglassesctrl lib.
            % 
            % CAUTION:
            % When you cannot discover TobiiProGlasses2 nevertheless it is connected on wifi or with a LAN cable,
            % please check your firewall settings. If the problem wasn't solved yet, try to restart your computer.

            % Import Java Library
            import java.io.*
            import java.net.*
            
            % Send Msg
            sendMsg = ['{"type":"discover", "date":"' char(datetime('now','TimeZone','local','Format','yyyy-MM-dd''T''HH:mm:ssZ')) '"}'];
            addr = InetAddress.getByName(obj.v6MULTICAST_ADDR);
            sendPacket = DatagramPacket(int8(sendMsg'), length(int8(sendMsg')), addr, obj.v6MULTICAST_PORT);
            intfs = NetworkInterface.getNetworkInterfaces();
            data = [];
            while intfs.hasMoreElements()
                intf = intfs.nextElement();
                socket = MulticastSocket(obj.v6MULTICAST_PORT);
                socket.setSoTimeout(obj.discoverTimeout);
                try
                    socket.setNetworkInterface(intf);  % java.net.SocketException: bad argument for IP_MULTICAST_IF2: No IP addresses bound to interface
                    socket.send(sendPacket);
                    fprintf('Discover request sent to (%s, %d) on interface %s. \n', obj.v6MULTICAST_ADDR, obj.v6MULTICAST_PORT, intf);
                    fprintf('Waiting response from a device ...\n\n');
                    % Receive Msg
                    recvPacket = DatagramPacket(zeros(1,obj.packetLength,'int8'), obj.packetLength);
                    socket.receive(recvPacket);  % java.net.SocketTimeoutException: Receive timed out
                    socket.close();
                    obj.host = char(recvPacket.getAddress());  % show ip address
                    data = recvPacket.getData();
                    data = jsondecode(char(data(1:recvPacket.getLength)'));
                catch                    
                    socket.close();
                end
                if ~isempty(data)
                    if contains(obj.host, '%')
                        s = split(obj.host, '%');
                        obj.host = s{1};
                    end
                    fprintf('Tobii Pro Glasses found with address: [%s]\n', obj.host);
                    break;
                end
            end
            if isempty(data)
                fprintf('Cannot find Tobii Glasses!\n');
            end
        end
        
        function setURL(obj)
            % like _set_URL_() in tobiiglassesctrl lib.

            if strcmp(obj.ipType, 'v4')
                obj.baseUrl = ['http://' obj.host];
            elseif strcmp(obj.ipType, 'v6')
                obj.baseUrl = ['http://[' obj.host ']'];
            end
        end
        
        function res = urlRequest(obj, req, data)
            % like _post_request_() and _get_request_() in tobiiglassesctrl lib.
            % 'data' should be String type. It is encoded to json format.

            url = [obj.baseUrl req];
            try
                switch nargin
                    case 2
                        % GET REQUEST
                        res = webread(url);
                    case 3
                        % POST REQUEST
                        option = weboptions('MediaType', 'application/json');
                        res = webwrite(url, data, option);
                end
            catch ME
                res = false;
                rethrow(ME);
            end
        end
        
        function status = connect(obj) 
            % like _connect_() in tobiiglassesctrl lib.

            if strcmp(obj.ipType, 'v4')||strcmp(obj.ipType, '')
                fprintf('Trying to find Tobii Pro Glasses 2 on wlan network...\n');
                obj.baseUrl = ['http://' obj.v4ADDR];
                try
                    status = obj.checkStatus('/api/system/status', 'sys_status', {'ok'});
                    if strcmp(status, 'ok')
                        obj.ipType = 'v4';
                        obj.host = obj.v4ADDR;
                        obj.setURL();
                        fprintf('Tobii Pro Glasses found with address: [%s]\n', obj.host);
                        fprintf('Tobii Pro Glasses 2 successful connected!\n');
                    else
                        error('An error occurs trying to connect to the Tobii Pro Glasses!');
                    end
                catch
                    fprintf('Not found on wlan network.\n');
                    obj.ipType = 'v6';
                end
            end
            if strcmp(obj.ipType, 'v6')
                obj.discover();
                obj.setURL();
                try
                    status = obj.checkStatus('/api/system/status', 'sys_status', {'ok'});
                    if strcmp(status, 'ok')
                        fprintf('Tobii Pro Glasses 2 successful connected!\n\n');
                    end
                catch
                    obj.ipType = '';
                    obj.close();
                    error('An error occurs trying to connect to the Tobii Pro Glasses!');
                end
            end
        end

        function udpSend(obj, socket, msg)
            % like socket.sendto() on python
            
            % Import Java Library
            import java.io.*
            import java.net.*
            
            if obj.streaming
                sendMsg = msg;
                addr = InetAddress.getByName(obj.host);
                packet = DatagramPacket(int8(sendMsg'), length(int8(sendMsg')), addr, obj.PORT);
                socket.send(packet);
            end
        end
        
        function sendKeepAliveMsg(obj)
            % like  __send_keepalive_msg__() in tobiiglassesctrl lib.
            
            t = timer;
            t.TimerFcn = @(~,~) obj.udpSend(obj.dataSocket, obj.KA_DATA_MSG);
            t.StopFcn = @(~,~) fprintf('Successfully stopped sending keep alive message.\n');
            t.Period = obj.keepAliveTimeout;    % sec.
            % t.TasksToExecute = Inf;  % default Inf
            t.ExecutionMode = 'fixedRate';
            start(t);
        end
        
        function fetchData(obj, socket)
            % like _grab_data_() in tobiiglassesctrl lib.
            
            % Import Java Library
            import java.io.*
            import java.net.*
            
            if obj.streaming
                packet = DatagramPacket(zeros(1,obj.packetLength,'int8'), obj.packetLength);
                socket.receive(packet);
                dat = packet.getData();
                strData = char(dat(1:packet.getLength)');
                obj.refreshData(strData);
            end
        end
        
        function keepRecvData(obj)
            % like _grab_data_() in tobiiglassesctrl lib.
            
            t2 = timer;
            t2.TimerFcn = @(~,~) obj.fetchData(obj.dataSocket);
            t2.StopFcn = @(~,~) fprintf('Successfully stopped receiving data.\n');
            t2.Period = 1/200; % sec. ideal sampling rate 100Hz * 7 (types of data)
            % t2.TasksToExecute = Inf;  % default Inf
            t2.ExecutionMode = 'fixedRate';
            start(t2);
        end

        function keepRecvDataFast(obj, strObj, funcType)
            % like _grab_data_() in tobiiglassesctrl lib.
            try
                obj.javaReceiver = RecvData(strObj, obj.dataSocket);
                if nargin == 2
                    if ~isempty(obj.recvFuncType)
                        funcType = obj.recvFuncType;
                    else
                        funcType = 2;
                    end
                end
                if funcType == 1
                    obj.javaReceiver.start();
                elseif funcType == 2
                    obj.javaReceiver.fetcher();
                end
            catch ME
                obj.close();
                rethrow(ME);
            end            
        end
        
        function status = waitForWlanStatus(obj, statusCellArray, timeout)
            % Wait until the recordong status turns to a specific state or timeout.
            % 
            % Syntax:
            %       status = obj.waitForWlanStatus([statusCellArray[, timeout]])
            % 
            %   'statusCellArray' - a cell array of strings represents status of the device.
            %                       Make sure to input value strings as a cell array (with ',' delimiter and enclosed with {})
            %   'timeout' - an integer to specify waiting time for responce in sec.
            % 
            %   'status' - one string in statusCellArray, otherwise 'timeout'. 
            % 

            if nargin == 1
                statusCellArray = {'enabling', 'ap', 'connecting', 'connected', 'not connected'};
                timeout = 10;
            elseif nargin == 2
                timeout = 10;
            end
            t = tic;
            while true
                status = obj.checkStatus('/api/system/status', 'sys_wlan.status', statusCellArray);
                if any(strcmp(status, statusCellArray))
                    break;
                elseif toc(t) > timeout
                    status = 'timeout';
                    break;
                end
            end
        end
    end         % methods (Access = private)

    methods (Access = private, Static)    
        function socket = mkSock()
            % like _mksock_() in tobiiglassesctrl lib.
            
            % Import Java Library
            import java.io.*
            import java.net.*
            
            socket = DatagramSocket();
        end
    end
end