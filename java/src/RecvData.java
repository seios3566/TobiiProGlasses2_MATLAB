import java.io.*;
import java.net.*;
import java.util.concurrent.*;

import matlabcontrol.*;         // https://github.com/jakaplan/matlabcontrol 
import matlabcontrol.MatlabProxy.MatlabThreadCallable;
import matlabcontrol.MatlabProxy.MatlabThreadProxy;
import matlabcontrol.extensions.*;


public class RecvData{
    String strObj;
    boolean isStreaming;
    double packetLength;
    DatagramSocket receiveSocket;
    MatlabProxyFactory factory;
    MatlabProxy proxy;

    public RecvData (String strObj, DatagramSocket sock) throws MatlabConnectionException, MatlabInvocationException
    {
        this.strObj = strObj;
        this.receiveSocket = sock;

        //Create a proxy, which we will use to control MATLAB
        factory = new MatlabProxyFactory();
        proxy = factory.getProxy();

        proxy.eval("global " + strObj);
        isStreaming = ((boolean[]) proxy.getVariable(strObj + ".streaming"))[0];
        packetLength = ((double[]) proxy.getVariable(strObj + ".packetLength"))[0];
    }

    class Receiver1 extends Thread{

        byte[] buf;
        DatagramPacket receivePacket;
        int len;
        String[] data;
        long startTime;

        public Receiver1() {
            buf = new byte[(int)packetLength];
            receivePacket = new DatagramPacket(buf, buf.length);
            data = new String[8];
        }

        @Override
        public void run(){
            try{ 
                while (isStreaming) {   
                    startTime = System.nanoTime();
                    data = new String[8];
                    for (int i = 0; i < data.length; i++) {
                        receiveSocket.receive(receivePacket);
                        len = receivePacket.getLength();
                        data[i] = new String(buf, 0, len-1);
                        if ((System.nanoTime() - startTime) > 8000000){
                            break;
                        }
                    }
                    proxy.eval(strObj + ".refreshData('[" + String.join(",", data)  + "]');");
                }
            }catch (Exception e) {
                System.out.print("Exception: ");
                e.printStackTrace();
            }
        }
    }

    class Receiver2 implements MatlabThreadCallable<Object> {

        byte[] buf;
        DatagramPacket receivePacket;
        int len;
        String[] data;
        long startTime;

        public Receiver2() {
            buf = new byte[(int)packetLength];
            receivePacket = new DatagramPacket(buf, buf.length);
            data = new String[8];
        }

        @Override
        public Object call(MatlabThreadProxy proxy) throws MatlabInvocationException {   
            try{ 
                if (isStreaming) {   
                    startTime = System.nanoTime();
                    for (int i = 0; i < data.length; i++) {
                        receiveSocket.receive(receivePacket);
                        len = receivePacket.getLength();
                        data[i] = new String(buf, 0, len-1);
                        if (System.nanoTime() - startTime > 8000000){
                            break;
                        }
                    }
                    proxy.eval(strObj + ".refreshData('[" + String.join(",", data)  + "]');");
                }
            }catch (Exception e) {
                System.out.print("Exception: ");
                e.printStackTrace();
            }
            return null;
        }
    }

    public void start(){
        new Receiver1().start();
    }

    public void stop(){
        isStreaming = false;
    }

    public void fetcher() {
        try {
            proxy.invokeAndWait(new Receiver2());
        } catch (MatlabInvocationException e) {
            isStreaming = false;
            System.out.print("Exception: ");
            e.printStackTrace();
        }
    }
}
