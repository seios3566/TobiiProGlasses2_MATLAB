If you want to modify the source code, you need to compile source files.
After edited, 2 way to use this function in MATLAB.
One way is to make .class file, the other way is additionally to compile .class file to make .jar file.

1 MAKE CLASS FILE:
cd <<THIS DIR>>
javac --release 8  -cp ".;.\src\matlabcontrol-4.1.0.jar" -sourcepath src -d classes src\RecvData.java

USAGE:
MATLAB > addjavapath('<<THIS DIR>>\src\matlabcontrol-4.1.0.jar');
MATLAB > addjavapath('<<THIS DIR>>');


After made class files, 
___________________________
<<THIS DIR>>
    classes
        matlabcontrol
            ...
        RecvData.class		<- made
        RecvData$Receiver.class <- made
    META-INF
        MANIFEST.MF
    src
        matlabcontrol.4.1.0.jar
        RecvData.java
_______________________________


2 MAKE JAR FILE:
jar cvfm recvDataFast.jar META-INF\MANIFEST.MF -C classes .

USAGE:
MATLAB > addjavapath('<<THIS DIR>>\recvDataFast.jar');



This java function is used in keepRecvDataFast() referred from startStreaming() of TobiiProGlasses2.m
obj = TobiiProGlasses2();
This is a constructor.
strObj is a character string of the name of the variable obj.

FUNC LIST:
obj.javaReceiver = RecvData(strObj, obj.dataSocket);
obj.javaReceiver.start();
obj.javaReceiver.fetcher();
obj.javaReceiver.stop();

